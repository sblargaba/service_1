Service 1
=========

The pipeline for this service is run every time there's a change on the master branch

Only a dummy Dockerfile is provided and no deployment files as they are not needed per the task

All the deployment is done trough the pipeline of the Service 2
